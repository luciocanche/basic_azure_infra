module "resourcegroup" {
  source = "../resourcegroup"
}

# Add ssh public key
resource "azurerm_ssh_public_key" "sshpk-centralus-dev-basicazureinfra" {
  name                = "sshpk-centralus-dev-basicazureinfra"
  resource_group_name = "${module.resourcegroup.rgname}"
  location            = "${module.resourcegroup.rglocation}"
  public_key          = file("~/.ssh/id_rsa.pub")
}

output "vpublickey" {
  value = azurerm_ssh_public_key.sshpk-centralus-dev-basicazureinfra.public_key
}
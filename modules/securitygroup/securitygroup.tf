module "resourcegroup" {
  source = "../resourcegroup"
}

# Add security group
resource "azurerm_network_security_group" "nsg-centralus-dev-basicazureinfra" {

  name                = "nsg-centralus-dev-basicazureinfra"
  resource_group_name = "${module.resourcegroup.rgname}"
  location            = "${module.resourcegroup.rglocation}"

  security_rule {
    name                       = "tcp-rule-inbound-ssh"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "tcp-rule-inbound-http"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "tcp-rule-outbound-all"
    priority                   = 120
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Development"
  }

}
output "securitygroupid" {
  value = azurerm_network_security_group.nsg-centralus-dev-basicazureinfra.id
}
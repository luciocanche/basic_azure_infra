module "resourcegroup" {
  source = "../resourcegroup"
}


# Adding a virtual network
resource "azurerm_virtual_network" "vnet-centralus-dev-basicazureinfra" {
  name                = "vnet-centralus-dev-basicazureinfra"
  address_space       = ["10.0.0.0/16"]
  resource_group_name = "${module.resourcegroup.rgname}"
  location            = "${module.resourcegroup.rglocation}"
}

# Adding a subnet
resource "azurerm_subnet" "snet-dev-basicazureinfra" {
  name                 = "snet-dev-basicazureinfra"
  resource_group_name = "${module.resourcegroup.rgname}"
  virtual_network_name = azurerm_virtual_network.vnet-centralus-dev-basicazureinfra.name
  address_prefixes     = ["10.0.1.0/24"]
}
output "subnetid" {
  value = azurerm_subnet.snet-dev-basicazureinfra.id
}










# Adding a route table
resource "azurerm_route_table" "rt-centralus-dev-basicazureinfra" {
  name                = "rt-centralus-dev-basicazureinfra"
  resource_group_name = "${module.resourcegroup.rgname}"
  location            = "${module.resourcegroup.rglocation}"
}
# Adding a route for the route table
resource "azurerm_route" "udr-dev-basicazureinfra-subnet" {
  name                = "udr-dev-basicazureinfra-subnet"
  resource_group_name = "${module.resourcegroup.rgname}"
  route_table_name    = azurerm_route_table.rt-centralus-dev-basicazureinfra.name
  address_prefix      = "10.0.0.0/16"
  next_hop_type       = "VnetLocal"
}
# Adding a route for the route table
resource "azurerm_route" "udr-dev-basicazureinfra-vngateway" {
  name                = "udr-dev-basicazureinfra-vngateway"
  resource_group_name = "${module.resourcegroup.rgname}"
  route_table_name    = azurerm_route_table.rt-centralus-dev-basicazureinfra.name
  address_prefix      = "0.0.0.0/0"
  next_hop_type       = "VirtualNetworkGateway"
}
resource "azurerm_subnet_route_table_association" "srta-dev-basicazureinfra" {
  subnet_id      = azurerm_subnet.snet-dev-basicazureinfra.id
  route_table_id = azurerm_route_table.rt-centralus-dev-basicazureinfra.id
}







# Adding a mandatory subnet for the virtual network gateway
resource "azurerm_subnet" "gatewaysubnet" {
  name                 = "GatewaySubnet"
  resource_group_name = "${module.resourcegroup.rgname}"
  virtual_network_name = azurerm_virtual_network.vnet-centralus-dev-basicazureinfra.name
  address_prefixes     = ["10.0.2.0/24"]
}
# Adding a public IP for Virtual Network Gateway
resource "azurerm_public_ip" "pip-centralus-dev-basicazureinfra-vng" {
  name                = "pip-centralus-dev-basicazureinfra-vng"
  resource_group_name = "${module.resourcegroup.rgname}"
  location            = "${module.resourcegroup.rglocation}"
  allocation_method = "Dynamic"
}
# Adding a virtual network gateway
resource "azurerm_virtual_network_gateway" "vgw-centralus-dev-basicazureinfra" {
  name                = "vgw-centralus-dev-basicazureinfra"
  resource_group_name = "${module.resourcegroup.rgname}"
  location            = "${module.resourcegroup.rglocation}"

  type     = "Vpn"
  vpn_type = "RouteBased"

  active_active = false
  sku           = "Basic"

  ip_configuration {
    name                          = "vnetGatewayConfig"
    public_ip_address_id          = azurerm_public_ip.pip-centralus-dev-basicazureinfra-vng.id
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.gatewaysubnet.id
  }
}

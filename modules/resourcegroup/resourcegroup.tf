# Create a resource group
resource "azurerm_resource_group" "rg-centralus-dev-basicazureinfra" {
  name     = "rg-centralus-dev-basicazureinfra"
  location = "Central US"
}

output "rgname" {
  value = azurerm_resource_group.rg-centralus-dev-basicazureinfra.name
}
output "rglocation" {
  value = azurerm_resource_group.rg-centralus-dev-basicazureinfra.location
}
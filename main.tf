# terraform graph | dot -Tsvg > graph.svg

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
  skip_provider_registration = true
}

module "resourcegroup" {
  source = "./modules/resourcegroup"
}


module "securitygroup" {
  source = "./modules/securitygroup"
}


module "vpc" {
  source="./modules/vpc"
}

module "security" {
  source = "./modules/security"
}




# Adding a NIC for the virtual machine
resource "azurerm_network_interface" "nic-centralus-dev-basicazureinfra" {
  name                = "nic-centralus-dev-basicazureinfra"
  location            = "${module.resourcegroup.rglocation}"
  resource_group_name = "${module.resourcegroup.rgname}"

  ip_configuration {
    name                          = "ipconfiguration1"
    subnet_id                     = "${module.vpc.subnetid}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.pip-centralus-dev-basicazureinfra-vm.id
  }
}
# Adding a public IP for the NIC of Virtual Machine
resource "azurerm_public_ip" "pip-centralus-dev-basicazureinfra-vm" {
  name                = "pip-centralus-dev-basicazureinfra-vm"
  location            = "${module.resourcegroup.rglocation}"
  resource_group_name = "${module.resourcegroup.rgname}"
  allocation_method = "Dynamic"
}
resource "azurerm_network_interface_security_group_association" "nisga" {
  network_interface_id      = azurerm_network_interface.nic-centralus-dev-basicazureinfra.id
  network_security_group_id = "${module.securitygroup.securitygroupid}"
}


# Adding a managed disk for the future image creation
#resource "azurerm_managed_disk" "osdisk-centralus-dev-basicazureinfra" {
#  name                 = "osdisk-centralus-dev-basicazureinfra"
#  location             = azurerm_resource_group.rg-centralus-dev-basicazureinfra.location
#  resource_group_name  = azurerm_resource_group.rg-centralus-dev-basicazureinfra.name
#  storage_account_type = "Standard_LRS"
#  create_option        = "Empty"
#  disk_size_gb         = 30
#}


# Adding a custom image from Virtual Hard Disk
# resource "azurerm_image" "imagevhd-centralus-dev-basicazureinfra" {
#   name                = "imagevhd-centralus-dev-basicazureinfra"
#   location            = azurerm_resource_group.rg-centralus-dev-basicazureinfra.location
#   resource_group_name = azurerm_resource_group.rg-centralus-dev-basicazureinfra.name

#   os_disk {
#     managed_disk_id = azurerm_managed_disk.osdisk-centralus-dev-basicazureinfra.id
#     os_type         = "Linux"
#     os_state        = "Generalized"
#     caching         = "ReadWrite"
#     size_gb         = 30
#   }
# }


## Adding a virtual machine
#resource "azurerm_virtual_machine" "vm-centralus-dev-basicazureinfra-1" {
#  name                  = "vm-centralus-dev-basicazureinfra-1"
#  location              = azurerm_resource_group.rg-centralus-dev-basicazureinfra.location
#  resource_group_name   = azurerm_resource_group.rg-centralus-dev-basicazureinfra.name
#  network_interface_ids = [azurerm_network_interface.nic-centralus-dev-basicazureinfra.id]
#  vm_size               = "Standard_DS1_v2"
#
#  # Uncomment this line to delete the OS disk automatically when deleting the VM
#  # delete_os_disk_on_termination = true
#
#  # Uncomment this line to delete the data disks automatically when deleting the VM
#  # delete_data_disks_on_termination = true
#
#  storage_image_reference {
#    #publisher = "Canonical"
#    #offer     = "UbuntuServer"
#    #sku       = "16.04-LTS"
#    #version   = "latest"
#    id = azurerm_image.imagevhd-centralus-dev-basicazureinfra.id
#  }
#
#  storage_os_disk {
#    name              = azurerm_managed_disk.osdisk-centralus-dev-basicazureinfra.name
#    caching           = "ReadWrite"
#    #create_option     = "Attach"
#    create_option     = "FromImage"
#    os_type           = "Linux"
#    #managed_disk_id   = azurerm_managed_disk.osdisk-centralus-dev-basicazureinfra.id
#    #managed_disk_type = "Standard_LRS"
#  }
#  os_profile {
#    computer_name  = "tflcanchedou"
#    admin_username = "testadmin"
#    admin_password = "Password1234!"
#  }
#  os_profile_linux_config {
#    disable_password_authentication = false
#    ssh_keys {
#      path     = "/home/testadmin/.ssh/authorized_keys"
#      key_data = azurerm_ssh_public_key.sshpk-centralus-dev-basicazureinfra.public_key
#    }
#  }
#  tags = {
#    environment = "staging"
#  }
#}



# Adding a virtual machine using the managed virtual machine from azure
resource "azurerm_linux_virtual_machine" "vm-centralus-dev-basicazureinfra" {
  name                  = "vm-centralus-dev-basicazureinfra"
  location            = "${module.resourcegroup.rglocation}"
  resource_group_name = "${module.resourcegroup.rgname}"
  size                  = "Standard_DS1_v2"
  admin_username        = "testadmin"
  network_interface_ids = [azurerm_network_interface.nic-centralus-dev-basicazureinfra.id]

  admin_ssh_key {
    username   = "testadmin"
    public_key = "${module.security.vpublickey}"
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}